import logging
import logging.config
import os

from django.conf import settings


def logger_injector(propagate=True):
    def wrapper(object_to_dec):
        if propagate:
            _inject_logger(object_to_dec)

        if not propagate:
            old_init = object_to_dec.__init__

            def new_init(self, *args, **kwargs):
                _inject_logger(self)
                old_init(self, *args, **kwargs)

            object_to_dec.__init__ = new_init

        return object_to_dec

    return wrapper


def _inject_logger(class_object, use_def_conf=True):
    obj_name = class_object.__class__.__name__
    if obj_name == 'type':
        obj_name = class_object.__name__

    setattr(class_object, 'logger', _create_logger(obj_name, use_def_conf))


def _create_logger(obj_name, use_def_conf):
    if use_def_conf:
        logger = logging.getLogger(obj_name)
        if not logger.hasHandlers():
            logger.setLevel(settings.LOGS_LEVEL)

            # FileHandler
            handlers = [logging.FileHandler(_log_path(obj_name))]

            # Console stderr handler
            if settings.LOGS_ON_STDERR:
                stderr_handler = logging.StreamHandler()
                handlers.append(stderr_handler)

            formatter = logging.Formatter(settings.LOGS_FORMAT)
            for handler in handlers:
                handler.setLevel(settings.LOGS_LEVEL)
                handler.setFormatter(formatter)
                logger.addHandler(handler)

    if not use_def_conf:
        logging.config.fileConfig('../config/logging.conf', False)
        logger = logging.getLogger(obj_name)  # musi istniec logger o nazwie identycznej do nazwy klasy

    return logger


def _log_path(obj_name):
    return os.path.join(settings.LOGS_PATH, "{}.log".format(obj_name))
