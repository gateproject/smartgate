import os
import subprocess
import shlex
import uuid

from django.conf import settings

from libs.utils.logger_injector import logger_injector


class TesseractError(Exception):
    """
    Custom exception, stores tesseract return status and error messages
    """

    def __init__(self, status, message):
        self.status = status
        self.message = message
        self.args = (status, message)


@logger_injector()
class TesseractWrapper:
    def __init__(self, tesseract_path,
                 tesseract_config='-psm 7 "{}"'.format(settings.TESSERACT_CONFIG_FILE_PATH)):
        self.tesseract_path = tesseract_path
        self.tesseract_config = tesseract_config

    def ocr_plate(self, image_path):
        """
        Calls Tesseract ocr.
        :param image_path: Input image file name
        :return: Tesseract output as string
        """
        output_file_name = os.path.join(os.path.dirname(image_path), "{}.txt".format(uuid.uuid4()))

        status, stdout, stderr = self._call_tesseract(image_path, output_file_name)

        self.logger.debug("Tesseract return={}".format(status))
        self.logger.debug("Tesseract stdout='{}'".format(stdout))
        self.logger.debug("Tesseract stderr='{}'".format(stderr))

        if status:
            errors = self._parse_stderr(stderr)
            raise TesseractError(status, errors)

        # opening Tesseract output file to read recognized license plate
        with open(output_file_name) as fp:
            recognized_license_plate = fp.read().replace(' ', '').replace('\n', '').strip()

        # removing Tesseract output file
        os.remove(output_file_name)

        self.logger.debug("Tesseract recognized plate='{}'".format(recognized_license_plate))
        return recognized_license_plate

    def _call_tesseract(self, input_filename, output_file_path):
        """
        Call tesseract as follows: tesseract_cmd input output config_files
        :param input_filename: temporary image file name
        :param output_file_path: text file name for tesseract to write results to
        :return: subprocess return code, stderr
        """
        # removing extension from output file path since tesseract adds '.txt'
        output_file_path = output_file_path[:output_file_path.rindex('.')]

        args = [self.tesseract_path, input_filename, output_file_path]

        if self.tesseract_config:
            args += shlex.split(self.tesseract_config)

        self.logger.debug("Calling Tesseract: {}".format(args))
        process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return process.wait(), process.stdout.read(), process.stderr.read()

    def _parse_stderr(self, stderr):
        """
        Parse error_string for error messages, return
        """
        error_lines = tuple(line for line in stderr.splitlines() if line.find('Error') >= 0)  # error in line

        return '\n'.join(error_lines) if len(error_lines) > 0 else stderr
