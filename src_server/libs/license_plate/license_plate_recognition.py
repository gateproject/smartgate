import os
import subprocess

from libs.license_plate.tesseract_wrapper import TesseractWrapper
from libs.utils.logger_injector import logger_injector


class LicensePlateRecognitionError(Exception):
    pass


class PlateDetectionError(LicensePlateRecognitionError):
    pass


@logger_injector()
class LicensePlateRecognition:
    def __init__(self, python2_path, plate_detection_script_path, tesseract_path):
        self.python2_path = python2_path
        self.plate_detection_script_path = plate_detection_script_path

        self.tesseract = TesseractWrapper(tesseract_path)

    def recognize(self, image_path):
        cropped_plate_image_path = self._crop_plate(image_path)

        return self.tesseract.ocr_plate(cropped_plate_image_path)

    def _crop_plate(self, image_path):
        args = [self.python2_path, self.plate_detection_script_path, image_path]

        self.logger.debug("Calling plate detection/crop system with args={}".format(args))
        process = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        if process.wait():
            msg = "Couldn't detect/crop license plate.\n" \
                  "stdout: {}\n" \
                  "stderr: {}".format(process.stdout.read(), process.stderr.read())
            self.logger.info(msg)
            raise PlateDetectionError(msg)

        file_name, file_ext = os.path.splitext(image_path)

        if not file_ext:
            file_ext = 'jpg'
        cropped_file_path = "{}-res.{}".format(file_name, file_ext)

        return cropped_file_path

    def _tesseract(self, image_path):
        return self.tesseract.ocr_plate(image_path)
