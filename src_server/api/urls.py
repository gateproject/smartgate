from django.conf.urls import include, url
from django.contrib import admin

from apps.smart_gate_auth.admin import user_admin_site


admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    url(r'^smart_gate/', include('apps.smart_gate.urls', namespace='smart_gate')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^plates-admin', include(user_admin_site.urls))
]

