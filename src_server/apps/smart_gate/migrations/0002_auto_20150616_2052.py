# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('smart_gate', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='licenseplate',
            name='company',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='device',
            name='company',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='device',
            name='license_plates',
            field=models.ManyToManyField(blank=True, to='smart_gate.LicensePlate'),
        ),
        migrations.AddField(
            model_name='authorizationlog',
            name='device',
            field=models.ForeignKey(null=True, to='smart_gate.Device'),
        ),
    ]
