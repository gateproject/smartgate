from django.conf.urls import url, include
from rest_framework import routers

from apps.smart_gate import views


router = routers.DefaultRouter()

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^validate_car_photo/$', views.ValidateCarPhoto.as_view()),
]