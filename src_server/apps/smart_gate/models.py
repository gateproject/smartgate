from django.db import models
from django.db.models.signals import post_save

from apps.smart_gate_auth.models import Company
from libs.utils.logger_injector import logger_injector


class LicensePlate(models.Model):
    company = models.ForeignKey(Company, null=False)
    plate_number = models.CharField(max_length=10, null=False, blank=False)

    def __str__(self):
        return self.plate_number


class Device(models.Model):
    company = models.ForeignKey(Company, null=False)
    address = models.CharField(max_length=100)
    inform_user = models.BooleanField()
    license_plates = models.ManyToManyField(LicensePlate, blank=True)

    def __str__(self):
        return "{}'s device, address: {}".format(self.company, self.address)


@logger_injector()
class AuthorizationLog(models.Model):
    plate_number = models.CharField(max_length=100)
    timestamp = models.DateTimeField()
    recognized = models.BooleanField()
    device = models.ForeignKey(Device, null=True)

    def __str__(self):
        return "plate number='{}', time={}, recognized={}"\
            .format(self.plate_number, self.timestamp, self.recognized)

def authorization_log_post_save(sender, instance, **kwargs):
    AuthorizationLog.logger.info("Created AuthorizationLog: {}".format(instance))

post_save.connect(authorization_log_post_save, sender=AuthorizationLog)
