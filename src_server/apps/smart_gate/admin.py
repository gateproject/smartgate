from django.contrib import admin

from apps.smart_gate.models import LicensePlate, AuthorizationLog, Device
from apps.smart_gate_auth.admin import user_admin_site


class ReadOnlyAdmin(admin.ModelAdmin):
    readonly_fields = []

    def get_readonly_fields(self, request, obj=None):
        return list(self.readonly_fields) + \
               [field.name for field in obj._meta.fields]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class AuthorizationLogAdmin(ReadOnlyAdmin):
    def get_queryset(self, request):
        if request.user.is_superuser:
            return AuthorizationLog.objects.all()
        return AuthorizationLog.objects.filter(device__company=request.user)


class LicensePlateAdmin(admin.ModelAdmin):
    exclude = ('company',)

    def get_queryset(self, request):
        if request.user.is_superuser:
            return LicensePlate.objects.all()
        return LicensePlate.objects.filter(company=request.user)

    def has_change_permission(self, request, obj=None):
        has_class_permission = super().has_change_permission(request, obj)
        if not has_class_permission:
            return False
        if obj is not None and not request.user.is_superuser and request.user.id != obj.company.id:
            return False
        return True

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.company = request.user
        obj.save()


class DeviceAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

    def get_queryset(self, request):
        if request.user.is_superuser:
            return Device.objects.all()
        return Device.objects.filter(company=request.user)

    def has_change_permission(self, request, obj=None):
        has_class_permission = super().has_change_permission(request, obj)
        if not has_class_permission:
            return False
        if obj is not None and not request.user.is_superuser and request.user.id != obj.company.id:
            return False
        return True

    def get_form(self, request, obj=None, **kwargs):
        self.exclude = [] if request.user.is_superuser else ['company']

        return super().get_form(request, obj, **kwargs)

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.company = request.user
        obj.save()


admin.site.register(LicensePlate, LicensePlateAdmin)
admin.site.register(Device, DeviceAdmin)
admin.site.register(AuthorizationLog, AuthorizationLogAdmin)

user_admin_site.register(LicensePlate, LicensePlateAdmin)
user_admin_site.register(Device, DeviceAdmin)
user_admin_site.register(AuthorizationLog, AuthorizationLogAdmin)
