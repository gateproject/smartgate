import os
import uuid

from django.conf import settings

from django.utils import timezone
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from rest_framework.views import APIView

from apps.smart_gate.models import LicensePlate, AuthorizationLog
from libs.license_plate.license_plate_recognition import LicensePlateRecognition
from libs.utils.logger_injector import logger_injector


@logger_injector()
class ValidateCarPhoto(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        self.logger.info("Request from user: {}".format(request.user))

        if len(request.FILES) != 1:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        name, parser = next(request.FILES.items())

        image_file_path = os.path.join(settings.MEDIA_ROOT, "{}_{}".format(uuid.uuid4(), name))

        with open(image_file_path, 'wb') as image_file:
            for line in parser.file:
                image_file.write(line)

        recognized_plate = LicensePlateRecognition(
            python2_path=settings.PYTHON2_BIN_PATH,
            plate_detection_script_path=settings.CROP_PLATE_PYTHON2_SCRIPT_PATH,
            tesseract_path=settings.TESSERACT_BIN_PATH
        ).recognize(image_file_path)

        if 'id' not in request.DATA and 'id' not in request.QUERY_PARAMS:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        device_id = request.DATA['id'][0] if 'id' in request.DATA else request.QUERY_PARAMS['id'][0]

        recognized = LicensePlate.objects.filter(
            device__company=request.user,
            plate_number=recognized_plate,
            device__id=device_id
        ).exists()

        AuthorizationLog.objects.create(plate_number=recognized_plate,
                                        timestamp=timezone.now(),
                                        recognized=recognized,
                                        device_id=device_id)

        return Response(recognized)
