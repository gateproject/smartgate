from rest_framework import serializers

from apps.smart_gate_auth.models import Company
from apps.smart_gate.models import Device, LicensePlate, AuthorizationLog


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ('address', 'company', 'inform_user')
        depth = 1


class LicensePlateSerializer(serializers.ModelSerializer):
    class Meta:
        model = LicensePlate
        fields = ('plate_number', 'device')


class AuthorizationLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuthorizationLog
        fields = ('plate_number', 'timestamp')
