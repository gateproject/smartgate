from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.admin.sites import AdminSite

from apps.smart_gate_auth.models import Company


class CompanyAdmin(UserAdmin):
    model = Company

    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('address',)}),
    )


class UserAdmin(AdminSite):
    login_form = AuthenticationForm
    index_template = 'admin/smart_gate_auth/UserAdmin/index.html'

    def has_permission(self, request):
        return request.user.is_active


user_admin_site = UserAdmin('platesadmin')


# by default Company user could add Groups so we unregister this feature
admin.site.unregister(Group)
admin.site.register(Company, CompanyAdmin)
user_admin_site.register(Company, CompanyAdmin)
