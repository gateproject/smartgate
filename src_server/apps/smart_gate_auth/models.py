from django.contrib.auth.models import AbstractUser, UserManager, Permission
from django.db import models


class Company(AbstractUser):
    address = models.CharField(max_length=100, blank=True)
    objects = UserManager()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        super().save(force_insert=False, force_update=False, using=None,
                     update_fields=None)

        permissions = Permission.objects.filter(
            name__in=('Can add license plate',
                      'Can change license plate',
                      'Can delete license plate',
                      'Can change authorization log',
                      'Can change device')
        )

        for perm in permissions:
            self.user_permissions.add(perm)
