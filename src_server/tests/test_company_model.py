from apps.smart_gate_auth.models import Company
from django.test import TestCase


class TestCompanyModel(TestCase):
    def setUp(self):
        Company.objects.create_user(username="test", password="test")
        self.user = Company.objects.get(username="test")

    def test_default_permissions(self):
        self.assertTrue(self.user.has_perm('smart_gate.add_licenseplate'))
        self.assertTrue(self.user.has_perm('smart_gate.change_licenseplate'))
        self.assertTrue(self.user.has_perm('smart_gate.delete_licenseplate'))
        self.assertTrue(self.user.has_perm('smart_gate.change_device'))
        self.assertTrue(self.user.has_perm('smart_gate.change_authorizationlog'))
