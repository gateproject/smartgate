from django.core.files.uploadedfile import SimpleUploadedFile
from apps.smart_gate_auth.models import Company
from apps.smart_gate.models import Device, LicensePlate
from django.test import TestCase, Client
import base64
import os


class TestValidateCarPhotoView(TestCase):
    def setUp(self):
        self.client = Client()
        self.url = '/smart_gate/validate_car_photo/'

        self.user = Company.objects.create_user(username="test", password="test")
        self.device = Device.objects.create(company=self.user, address="test_address", inform_user=False)
        self.plate = LicensePlate.objects.create(company=self.user, plate_number="Z1SABAT")

        self.device.license_plates.add(self.plate)

        self.example_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Z1SABAT.jpg')

    def test_good_authorization_valid_car(self):
        response = self.client.post(self.url,
                                    self.__get_post_params(self.example_file_path),
                                    **self.__get_auth_headers())

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'true')

    def test_good_authorization_invalid_car(self):
        self.plate.delete()

        response = self.client.post(self.url,
                                    self.__get_post_params(self.example_file_path),
                                    **self.__get_auth_headers())

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'false')

    def test_bad_authorization(self):
        response = self.client.post(self.url,
                                    self.__get_post_params(self.example_file_path),
                                    **self.__get_auth_headers(pwd='asdasdsad'))

        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.content, b'{"detail":"Invalid username/password."}')

    def test_good_authorization_no_params(self):
        response = self.client.post(self.url, **self.__get_auth_headers())

        self.assertEqual(response.status_code, 400)

    def test_good_authorization_no_image(self):
        response = self.client.post(self.url, {'id': self.device.id}, **self.__get_auth_headers())

        self.assertEqual(response.status_code, 400)

    def test_good_authorization_no_device_id(self):
        response = self.client.post(self.url,
                                    self.__get_post_params(self.example_file_path, device_id=None),
                                    **self.__get_auth_headers())

        self.assertEqual(response.status_code, 400)

    def __get_auth_headers(self, login='test', pwd='test'):
        bytes_credentials = bytes('{}:{}'.format(login, pwd), encoding='ascii')

        return {
            'HTTP_AUTHORIZATION': 'Basic {}'.format(base64.b64encode(bytes_credentials).decode('utf-8'))
        }

    def __get_post_params(self, img_path, device_id=1):
        if device_id:
            device_id = self.device.id

        with open(img_path, 'rb') as fp:
            kwargs = {'image': SimpleUploadedFile('image', fp.read(), content_type="image/jpeg")}

        if device_id:
            kwargs['id'] = device_id

        return kwargs
