import os

from django.conf import settings
from django.test import TestCase

from src_server.libs.license_plate.license_plate_recognition import LicensePlateRecognition


class TestLicensePlateRecognition(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestLicensePlateRecognition, cls).setUpClass()

        cls.lpr = LicensePlateRecognition(
            python2_path=settings.PYTHON2_BIN_PATH,
            plate_detection_script_path=settings.CROP_PLATE_PYTHON2_SCRIPT_PATH,
            tesseract_path=settings.TESSERACT_BIN_PATH
        )
        cls.test_photos_path = os.path.join(os.path.dirname(settings.BASE_DIR), 'test_photos')
        cls.remove_res_images = []

    @classmethod
    def tearDownClass(cls):
        super(TestLicensePlateRecognition, cls).tearDownClass()

        for file_path in cls.remove_res_images:
            os.remove(file_path)

    def test_recognize_images(self):

        expected_lprs = []
        recognized_lprs = []
        files_list = list(os.listdir(TestLicensePlateRecognition.test_photos_path))

        for image_file_name in files_list:
            image_path = os.path.join(TestLicensePlateRecognition.test_photos_path, image_file_name)

            expected_lpr, file_ext = os.path.splitext(os.path.basename(image_path))

            expected_lprs.append(expected_lpr)
            recognized_lprs.append(TestLicensePlateRecognition.lpr.recognize(image_path))

            TestLicensePlateRecognition.remove_res_images.append(image_path.replace('.jpg', '-res..jpg'))

        different_values = [i for i, j in zip(expected_lprs, recognized_lprs) if i != j]
        self.assertEqual(expected_lprs, recognized_lprs, "Errors: {}".format(different_values))
