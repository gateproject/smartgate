# README #

## What is SmartGate?

SmartGate is a web application that could be used by a parking renting company with automatic gate opening based on automatic license plate recognition (first the image is preprocessed with OpenCV, then the license plate recognition is done with [openalpr](https://github.com/openalpr/openalpr)).

The license plate recognition is available for clients of the company through a REST API endpoint.

The company can add/modify/delete clients through an admin panel.


The project has been written for Python in the Enterprise university course at AGH University of Science and Technology in Poland.

It has been written in Python using Django & django-rest-framework and is covered with unit tests.

After the release it hasn't been developped anymore.

Flow chart:
![data_flow.png](https://bitbucket.org/repo/g9GXgL/images/47131768-data_flow.png)

Use case diagram:
![use_case_diagram.png](https://bitbucket.org/repo/g9GXgL/images/1718823552-use_case_diagram.png)


## Old stuff / used for development

To start working on the project, take an issue from [issues tab](issues).

For more information about project - check our [WIKI](wiki).

Recommendations:

* stick to [PEP 8 - coding style guide](https://www.python.org/dev/peps/pep-0008/) - but lets change the 80 characters length limit to 120

* use [virtualenv](https://virtualenv.pypa.io/en/latest/) - tool to create virtual environments for Python projects

* * if you are going to launch the project using console instead of ''run' option in IDE, it's good to use [virtualenvwrapper](https://virtualenvwrapper.readthedocs.org/en/latest/)

* if you don't have any Python IDE, try [PyCharm](https://www.jetbrains.com/pycharm/) - probably the best IDE for Python (professional version is free for students)