"""
License plate detection & crop system.

Usage:
  plate_detection <image_path>
  plate_detection -h | --help

Options:
    -h --help       Show this screen.
    <image_path>    Path to image file.
"""
import cv2
import numpy as np
import docopt
import os


def detect_plate(file_path):
    img = cv2.imread(file_path)
    blur = cv2.GaussianBlur(img, (13, 13), 1)

    gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)

    dilated = cv2.dilate(gray, cv2.getStructuringElement(cv2.MORPH_RECT, (9, 9)))
    eroded = cv2.erode(dilated, cv2.getStructuringElement(cv2.MORPH_RECT, (9, 9)))

    diff = cv2.absdiff(eroded, gray)
    sobel = cv2.Sobel(diff, cv2.CV_8U, 1, 0, 3, 1, 0.4, cv2.BORDER_DEFAULT)

    blur = cv2.GaussianBlur(sobel, (5, 5), 2)
    dilated2 = cv2.dilate(blur, cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3)))

    morph = cv2.morphologyEx(dilated2, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (30, 3)))

    threshold = cv2.threshold(morph, 0, 255, cv2.THRESH_OTSU + cv2.THRESH_BINARY)[1]

    contures = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[1]

    cnt = []

    for c in contures:
        minrect = cv2.minAreaRect(c)
        centre, width_and_height, theta = minrect
        if width_and_height[1] == 0.0:
            continue

        aspect_ratio = float(max(width_and_height) / min(width_and_height))

        if aspect_ratio < 2.76:
            continue

        if c[0][0][1] < img.shape[0] / 4:
            continue

        cnt.append(c)

    mr = cv2.minAreaRect(max(cnt, key=lambda x: cv2.contourArea(x)))
    box = cv2.boxPoints(mr)

    dx = 6  # changed from 25, so the plate takes up most of output image
    p1 = list(map(lambda x: x - dx, min(box, key=lambda x: x[0] + x[1])))
    p2 = list(map(lambda x: x + dx+dx, max(box, key=lambda x: x[0] + x[1])))

    plate = img[p1[1]:p2[1], p1[0]:p2[0]]
    '''
    At this point it is assumed license plate in in the centre of the image
    There's also noise and plenty of useless objects
    Those need to be removed for tesseract to work correctly
    '''

    '''
    What happens next:
    1. Manipulate brightness - thresh prepartion
    2. Resize, threshold - Otsu
    3. Delete tiny and huge shapes
    4. Dilate/erode - noise reduction
    5. Crop
    '''
    plate = cv2.fastNlMeansDenoising(plate, None, 10, 7, 21)
    plate_g = cv2.cvtColor(plate, cv2.COLOR_BGR2GRAY)

    '''
    # remove bluish fragments
    low_blue = np.array([150, 20, 20], dtype=np.uint8)
    high_blue = np.array([255, 200, 100], dtype=np.uint8)
    mask_blue = cv2.inRange(plate, low_blue, high_blue)
    plate_no_blue = cv2.add(plate_g, mask_blue)
    '''
    # brightness manipulation
    plate_no_blue = cv2.add(plate_g, -30)

    # scale
    res_f = 2
    plate_res = cv2.resize(plate_no_blue, None, fx=res_f, fy=res_f, interpolation=cv2.INTER_CUBIC)

    # threshold
    ret, plate_otsu = cv2.threshold(plate_res, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

    # invert
    plate_otsu = 255 - plate_otsu
    plate_morphed = plate_otsu.copy()

    # image info
    wid, hei = plate_res.shape
    img_area = wid * hei

    # mask for shape deletion
    mask_shape_del = np.zeros(plate_otsu.shape, np.uint8)

    contours = cv2.findContours(plate_otsu.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[1]
    c_to_delete = []
    # params
    min_cnt_area = 20
    for c in contours:
        if cv2.contourArea(c) < min_cnt_area:
            c_to_delete.append(c)
        if cv2.contourArea(c) > 0.075*img_area:
            c_to_delete.append(c)
        pass
    # draw selected contours
    cv2.drawContours(mask_shape_del, c_to_delete, -1, 255, -1)  #-1=all, 255=white, -1=thickness
    # delete shapes within contours
    #cv2.bitwise_not(plate_otsu, plate_otsu, mask=mask_shape_del)
    plate_otsu = cv2.bitwise_and(plate_otsu, 255-mask_shape_del)

    # invert back
    plate_otsu = 255 - plate_otsu

    # morph - dilate & erode, noise reduction
    plate_otsu = cv2.morphologyEx(plate_otsu.copy(), cv2.MORPH_CLOSE, np.ones((3, 3), np.uint8))

    # crop
    cut = 8  # pixels to cut at borders
    plate_otsu = plate_otsu[cut:wid-cut, cut:hei-cut]

    file_name, file_ext = os.path.splitext(file_path)

    if not file_ext:
        file_ext = 'jpg'

    cropped_file_path = "{}-res.{}".format(file_name, file_ext)
    cv2.imwrite(cropped_file_path, plate_otsu)

    # cv2.imwrite(file_path[:-4] + "-pre_cnt.jpg", plate_morphed)
    # cv2.imwrite(file_path[:-4] + "-mask.jpg", mask_shape_del)

    return cropped_file_path  # detected plate file name



if __name__ == '__main__':
    args = docopt.docopt(__doc__)

    detect_plate(args['<image_path>'])
